# Thermal_RPS


## Thermal RPS
A Rock-Paper-Scissor images dataset based on a thermal camera as an example task for edge AI applications.

## Description
Images are acquired using Adafruit AMG8833 thermal camera and binarized in order to be applicable for the BNN. The dataset contains 42 images where the class-wise split is 15, 13 and 14 images for  `Rock' ,` Paper' and `Scissors' respectively.

## Dataset samples

![Alt text](Thermal_RPS/0.jpg?raw=true "Rock")
![Alt text](Thermal_RPS/40.jpg?raw=true "Paper")
![Alt text](Thermal_RPS/20.jpg?raw=true "Scissor")


## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Authors and acknowledgment
If you use the dataset as part of your work please cite us using the following:
@misc{Parmar_2022,
  author = {V. Parmar, S.K. Kingra, S.Negi, M.Suri},
  title = {Thermal RPS dataset},
  year = {2022},
  publisher = {GitLab},
  journal = {GitLab repository},
  howpublished = {\url{https://github.com/NVM_IITD_Research/thermal_rps}},
}

 
## License
Copyright (c) 2022 NVM_IITD_Research

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
